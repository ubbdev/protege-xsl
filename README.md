# Protege transformations

Protege transformations contains two stylesheets which are used to post process the owl data 
created when saving a Protege 3.5 project. This module is used by our Protege setup, and also our protege-sparql-update project.
The functionality these stylesheets provide can be summarised to:
Tests written to check functionality are written in Xspec and can be found in **test/** folder.

## rewriteProtegeUris.xsl

- rewriting Class URIs based on the value of rdf:type and dct:identifer, for objects which have a rdf:type and dct:identifier. Ex http://data.ub.uib.no/photograph_1 which has a dct:identifier of ubb-kk-1500-1 is rewritten to 
- http://data.ub.uib.no/instance/photograph/ubb-kk-1500-1. There are some exceptions such as if the uri already exists, and also if there are several objects with the same new uri (in wait to be written), messages are output to terminal instead, informing of the uri collison.
- `rdf:datatype="...string"` is added to all literal properties which does not have an existing rdf:datatype.  This to ensure serialization to be the same for new jena output (when running sparql-protege-update) to help inspecting the difference between datasets.

## mergeWith.xsl
-  Check for property <item1> ubbont:mergeWith <item2> . Item1 will have its uri rewritten to the uri of item2, which will merge the data. Also dct:identifier is dropped and replaced with previousIdentifier. All pointers to item1 will also be changed to point at item2. To keep the history of the item and its previous name we add also the following items:
```
   <ubbont:hasBeenMergedWith><xsl:value-of select="@rdf:about"/></ubbont:hasBeenMergedWith>
                <xsl:if test="exists(dct:identifier)">
                <ubbont:previousIdentifier><xsl:value-of select="dct:identifier"/></ubbont:previousIdentifier>                
                </xsl:if>
```
- Delete xml:lang attributes with illegal content. Illegal lang elements can be saved in protege, but can't be loaded into a tdb.
- Check for property `dce:relation` where there exists an objects with it's id. If item1. dct:relation "ubb-ms-0001" and item2 dct:identifier ubb-ms-0001, we construct a relationship between the items based on their classes, to replace the dce:relation string.
- We sort the output based on  this ordering:
```
  <xsl:apply-templates>
<!--                <xsl:with-param name="identifiers" select="$identifiers" tunnel="yes"/>
-->                <!-- top level sort when context rdf:RDF-->
                <xsl:sort select="@rdf:about"/>
                <!--- element name-->
                <xsl:sort select="name()"/>
                <xsl:sort select="@rdf:resource"/>
                <!-- string value of -->
                <xsl:sort select="."/>
            </xsl:apply-templates>
```

@Todo Create an ant task for running these stylesheets.