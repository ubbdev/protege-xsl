<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE urls SYSTEM "../common/entitites.dtd">
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:dct="http://purl.org/dc/terms/"
    xmlns:flub="http://ub.uib.no/flibub"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:ubbont="http://data.ub.uib.no/ontology/"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:key name="used-id" match="*:item" use="@new_uri"/>
    <xsl:key name="used-id" match="rdf:Description" use="@rdf:about" />
    <xsl:key name="matching-id" match="*:item" use="@old_uri"/> 
    <xsl:variable name="current-document" as="node()">
        <xsl:value-of select="/"/>
    </xsl:variable>
    
    <xsl:template match="rdf:RDF">
       
        <xsl:variable name="identifiers">   
            <root>
                <xsl:for-each select="descendant::rdf:Description/dct:identifier">
                    <xsl:variable name="new-uri" select="lower-case(concat(flub:getClassNameFromUri(parent::rdf:Description/rdf:type[1]/@rdf:resource),'/',encode-for-uri(.)))"/>
                    <xsl:variable name="type-uri" select="parent::rdf:Description/rdf:type[1]/@rdf:resource"/>
                    <xsl:variable name="current-uri" select="parent::rdf:Description/@rdf:about"/>
                    <xsl:if test="$type-uri and $new-uri != $current-uri and (matches($current-uri,'data\.ub\.uib\.no|file:') or count(tokenize($current-uri,'/'))=1)">    
                        <item old_uri="{parent::rdf:Description/@rdf:about}" new_uri="{lower-case(concat(flub:getClassNameFromUri($type-uri),'/',encode-for-uri(.)))}"/>
                    </xsl:if>
                    <xsl:if test="not($type-uri)">
                        <xsl:message>Warning no type for resource: <xsl:value-of select="parent::*/@rdf:about"/> dct:identifier <xsl:value-of select="."/></xsl:message>
                    </xsl:if>
                </xsl:for-each>
            </root>
            
        </xsl:variable>
        <xsl:copy>
        <xsl:copy-of select="@*"/>
            <xsl:apply-templates>
            <xsl:with-param name="identifiers" tunnel="yes" select="$identifiers"/>
        </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
   
    
    
    <xsl:template match="/">        
        <xsl:apply-templates/>     
    </xsl:template>
    
        
    <xsl:template match="*">
       <xsl:param name="identifiers" tunnel="yes"/>
        <xsl:copy>
            <xsl:for-each select="@*">
                <xsl:variable name="current" select="."/>
                <xsl:variable name="new_uri" select="$identifiers/descendant::root/item[@old_uri=$current]/@new_uri"/>
                <xsl:variable name="matching-id" select="$identifiers/key('matching-id',$current)"/>
                <xsl:choose>
                    <!-- should be xsl key-->
                    <xsl:when test="($identifiers/key('matching-id',$current) and key('used-id',$new_uri)) or count(key('used-id',$new_uri,$identifiers)) &gt; 1">
                        <xsl:message>
                            <xsl:value-of select="."/><xsl:text> has existing identifier and is a mismatch! See old uri:</xsl:text><xsl:value-of select="$current"/><xsl:text> new uri</xsl:text><xsl:value-of select="$new_uri"/> <xsl:text> (dct:identifier)</xsl:text>
                        </xsl:message>
                        
                        <xsl:copy-of select="."/>
                        
                    </xsl:when>
                    <xsl:when test="$matching-id">
                        <xsl:attribute name="{name(.)}">
                            <xsl:value-of select="$matching-id[1]/@new_uri"/>
                        </xsl:attribute>  
                    </xsl:when>
                   <xsl:when test="name(.)='rdf:datatype' and string(.)='http://www.w3.org/2001/XMLSchema#integer'">
                   	<xsl:attribute name="rdf:datatype" select="'http://www.w3.org/2001/XMLSchema#int'"/>
                   </xsl:when> 
                    <xsl:otherwise>
                        <xsl:copy-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <!-- inserting datatype property string, for same serialization as old jena--> 
            <!-- second statement empty properties-->       
            <xsl:if test="(not(@xml:lang) and string(.) and not(@rdf:datatype) and not(*))
                       or (not(@*) and(not(string(.)))
                       )">
                             <xsl:attribute name="rdf:datatype" select="'http://www.w3.org/2001/XMLSchema#string'"/>                           
                        </xsl:if>          
            <xsl:apply-templates>
                <xsl:with-param name="identifiers" select="$identifiers" tunnel="yes"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    
    
    <!--@todo lookup english label instead, CamelCase on space. Function exists somewhere-->
    <xsl:function name="flub:getClassNameFromUri">
        <xsl:param name="uri" as="xs:string"/>
        <!-- defensive check to see if uri is a valid uri-->
        <xsl:if test="not($uri castable as xs:anyURI)">
            <xsl:message terminate="yes">illegal uri </xsl:message>
        </xsl:if>
        <!-- handling exceptions-->
        <xsl:choose>
            <xsl:when test="$uri='http://www.w3.org/2004/02/skos/core#Concept'">
                <xsl:value-of select="'http://data.ub.uib.no/topic'"/>
            </xsl:when>
            <xsl:when test="$uri='http://www.w3.org/2004/02/skos/core#ConceptScheme'">
                <xsl:value-of select="'http://data.ub.uib.no/conceptscheme'"/>
            </xsl:when>
            <xsl:when test="$uri='http://data.ub.uib.no/ontology/ProxyCollection'">
                <xsl:value-of select="'http://data.ub.uib.no/instance/collection'"/>
            </xsl:when>
            <xsl:when test="$uri='http://data.ub.uib.no/ontology/Exhibition'">
                <xsl:value-of select="'http://data.ub.uib.no/exhibition'"/>
            </xsl:when>
            <xsl:when test="matches($uri,'/$')">
                <xsl:variable name="tokens" select="tokenize($uri,'/')" as="xs:string+"/>                
                <xsl:if test="count($tokens) &lt; 4">
                    <xsl:message terminate="yes">
                        a uri without a local part cannot be used as class name due to naming. <xsl:value-of select="$uri"/>
                    </xsl:message>                    
                </xsl:if>
                <xsl:value-of select="concat('&ubbinst;',$tokens[last()-1])"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('&ubbinst;',tokenize(replace($uri,'^.+?/([^/]+)$','$1'),'#')[last()])"/></xsl:otherwise>
        </xsl:choose>        
    </xsl:function>
    
    
</xsl:stylesheet>
