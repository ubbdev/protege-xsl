<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE urls SYSTEM "../common/entitites.dtd">
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:dct="http://purl.org/dc/terms/"
    xmlns:flub="http://ub.uib.no/flibub"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:ubbont="http://data.ub.uib.no/ontology/"
    xmlns:dce="http://purl.org/dc/elements/1.1/"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:param name="debug" as="xs:boolean" select="true()"/>
    <xsl:output indent="yes"/>
    <!-- removing all space to re-sort on result set-->
    <xsl:strip-space elements="*"/>    
    <!-- Stylesheet to perform a merge from property ubbont:mergeWith on an jena owl ontology-->
    
    <!--@keys-->
    <!-- key for selecting rdf:Description element based on value of rdf:about-->
    <xsl:key name="lookup-by-uri" match="*" use="@rdf:about"/>    
    <!-- key for selecting item in table found inside the variable $identifiers-->    
    <xsl:key name="lookup-by-identifier-uri" match="item" use="@old_uri"/>
    <xsl:key name="lookup-by-dct-identifier" match="*" use="dct:identifier"></xsl:key>
  
  <xsl:template match="rdf:RDF" priority="1.5">
      <!-- identifiers makes a xml of type:
        <root>
            <item old_uri="http://old.uri" new_uri="http://new.uri"/>        
            <ite...
        </root>
        for all instances of mergeWith in the dataset-->
      <xsl:variable name="identifiers">   
          <root>
              <xsl:for-each select="descendant::rdf:Description/ubbont:mergeWith">
                  <xsl:variable name="new-uri" select="@rdf:resource"/>        
                  <xsl:if test="$debug">    <xsl:message>
                      Identifiers item new
                      <xsl:value-of select="$new-uri"/> old  
                      <xsl:value-of select="ancestor::rdf:Description/@rdf:about"/>
                  </xsl:message>
                  </xsl:if>
                  <item old_uri="{ancestor::rdf:Description/@rdf:about}" new_uri="{$new-uri}"/>                
              </xsl:for-each>
          </root>   
      </xsl:variable>
      <xsl:copy>
          <xsl:copy-of select="@*"/>
      <xsl:apply-templates>
          <xsl:with-param name="identifiers" select="$identifiers" tunnel="yes"/>
          <xsl:sort select="@rdf:about"/>
          <!--- element name-->
          <xsl:sort select="name()"/>
          <xsl:sort select="@rdf:resource"/>
          <!-- string value of -->
          <xsl:sort select="."/>
      </xsl:apply-templates>
      </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*[@xml:lang and not(matches(@xml:lang,'^[a-z][a-z][a-z]?$'))]" priority="3.0">
    <xsl:copy>
    <xsl:copy-of select="@* except @xml:lang"/>
    <xsl:apply-templates xml:space="preserve"/>
    </xsl:copy>
    </xsl:template>
        
 <!-- exception for items that has text children and no element children (Not literal or XML-literal, but plain datatype values like int, string, etc. -->
    <xsl:template match="*[text() and not(*)]" priority="1.5">        
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates xml:space="preserve"/>
        </xsl:copy>    
    </xsl:template>
        
    <xsl:template match="dce:relation[key('lookup-by-dct-identifier',.)/@rdf:about]" priority="2.5">
        <xsl:variable name="predicate" select="flub:getPredicateInversePredicateFromType(parent::*/rdf:type/@rdf:resource)"/>
        <xsl:element name="{$predicate[1]}">
      
            <rdf:Description rdf:about="{key('lookup-by-dct-identifier',.)/@rdf:about}">
               <xsl:element name="{$predicate[2]}"> 
                   <xsl:attribute name="rdf:resource" select="parent::*/@rdf:about"/>
               </xsl:element>
            </rdf:Description>
        </xsl:element>
    </xsl:template>
    
    <xsl:function name="flub:getPredicateInversePredicateFromType">
        <xsl:param name="type"/>
        <xsl:choose>
            <xsl:when test="$type='http://www.w3.org/2004/02/skos/core#Concept'">
                <xsl:sequence select="'ubbont:isSubjectOf','dct:subject'"/>                
            </xsl:when>
            <xsl:when test="$type='http://www.w3.org/2003/01/geo/wgs84_pos#SpatialThing'">
                <xsl:sequence select="'ubbont:locationFor','dct:spatial'"/>
            </xsl:when>
            <xsl:otherwise><xsl:sequence select="'dct:relation','dct:relation'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- rule for match which contains mergeWith prop-->
    <xsl:template match="rdf:Description[ubbont:mergeWith]" priority="2.0">
        <xsl:if test="$debug">
            <xsl:message>mergeWith rdf:resource<xsl:value-of select="ubbont:mergeWith/@rdf:resource"/></xsl:message>
        </xsl:if>
       <xsl:variable name="rdf:type" select="rdf:type/@rdf:resource"/>
        <xsl:if test="$debug">
            <xsl:message>rdf:type <xsl:value-of select="$rdf:type"/></xsl:message>
        </xsl:if>
        <!-- start of current element copy-->
       <xsl:copy>
        <xsl:choose>
            <!-- enforce one mergeWith only and same type on properties which are to be merged-->
            <xsl:when test="key('lookup-by-uri',ubbont:mergeWith/@rdf:resource)/rdf:type/@rdf:resource=$rdf:type and string($rdf:type) and count(ubbont:mergeWith/@rdf:resource)=1">
                <xsl:if test="$debug"> <xsl:message>when</xsl:message></xsl:if>
                <xsl:attribute name="rdf:about" select="ubbont:mergeWith/@rdf:resource"/>
                <!-- skip dct identifier and mergeWith-->
                <xsl:apply-templates select="* except (dct:identifier,ubbont:mergeWith)">
                    <xsl:sort select="name()"/>
                    <xsl:sort select="@rdf:resource"/>
                    <xsl:sort select="text()"/>           
                </xsl:apply-templates>                
                <!--Create properties for provenance-->
                <ubbont:hasBeenMergedWith><xsl:value-of select="@rdf:about"/></ubbont:hasBeenMergedWith>
                <xsl:if test="exists(dct:identifier)">
                <ubbont:previousIdentifier><xsl:value-of select="dct:identifier"/></ubbont:previousIdentifier>                
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <!-- Gracefully copy attribute content and children if switch conditions are not met = No merge-->
                <xsl:message>Tried to merge with no matching CLASS type <xsl:value-of select="$rdf:type"/> <xsl:value-of select="@rdf:about"/> <xsl:value-of select="ubbont:mergeWith/@rdf:resource"/></xsl:message>          
               <xsl:copy-of select="@*"/>
               <xsl:apply-templates/>           
            </xsl:otherwise>
        </xsl:choose>
       </xsl:copy>
    </xsl:template>    
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- rule to handle any (leftover) matches-->    
    <xsl:template match="*" priority="1.0">
        <xsl:param name="identifiers" tunnel="yes" as="node()"/>
        <!-- start copy of element-->
        <xsl:copy>
            <!-- copy attribute nodes, except links-->
            <xsl:copy-of select="@* except (@rdf:resource,@rdf:about)"/>
            <!-- checks if current node is in the global list of $identifiers which are to to be replaced and selects to variable lookup-->
            <xsl:variable name="lookup" select="key('lookup-by-identifier-uri',(@rdf:resource,@rdf:about)[1],$identifiers)"/>
        <xsl:choose> 
            <xsl:when test="$lookup">
                <xsl:attribute name="{if (@rdf:about) then 'rdf:about' else 'rdf:resource'}" select="$lookup/@new_uri"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="(@rdf:resource,@rdf:about)"/>
            </xsl:otherwise>
        </xsl:choose>  
            <xsl:apply-templates>
<!--                <xsl:with-param name="identifiers" select="$identifiers" tunnel="yes"/>
-->                <!-- top level sort when context rdf:RDF-->
                <xsl:sort select="@rdf:about"/>
                <!--- element name-->
                <xsl:sort select="name()"/>
                <xsl:sort select="@rdf:resource"/>
                <!-- string value of -->
                <xsl:sort select="."/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>    
</xsl:stylesheet>
